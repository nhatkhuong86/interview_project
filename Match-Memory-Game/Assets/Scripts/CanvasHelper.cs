﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]
public class CanvasHelper : MonoBehaviour {

#if UNITY_EDITOR
    int preWidth, preHeight;
#endif

    void Awake()
    {
        var size3_2 = 3/2f;
        var sizeScene = (float)Screen.width/Screen.height;
        var isMatchHeight = sizeScene >= size3_2;
        var canvas = GetComponent<CanvasScaler>();
        canvas.matchWidthOrHeight = isMatchHeight ? 1 : 0;
    }

#if UNITY_EDITOR
    void Update()
    {
        if (preWidth != Screen.width || preHeight != Screen.height)
        {
            preWidth = Screen.width;
            preHeight = Screen.height;
            Awake();
        }        
    }
#endif
}
